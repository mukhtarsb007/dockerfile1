# FROM openjdk:17
# WORKDIR /app
# COPY ./ /app
# WORKDIR /app
# EXPOSE 8080
# CMD ["java", "-jar", "hallo-0.0.1-SNAPSHOT.jar"]
# FROM ubuntu:18.04

# RUN apt-get update && apt-get install -y openjdk-17-jdk maven git

# RUN git clone  https://gitlab.com/test9613/4_dockerfile /app

# WORKDIR /app

# CMD ["mvn", "exec:java"]

FROM eclipse-temurin:17-jdk-alpine as builder 
WORKDIR /opt/app 
COPY .mvn/ .mvn 
COPY mvnw pom.xml ./ 
RUN ./mvnw dependency:go-offline 
COPY ./src ./src 
RUN ./mvnw clean install 
 
FROM eclipse-temurin:17-jre-alpine 
WORKDIR /opt/app 
COPY --from=builder /opt/app/target/*.jar /opt/app/*.jar 
EXPOSE 8080 
ENTRYPOINT ["java", "-jar", "/opt/app/*.jar"]